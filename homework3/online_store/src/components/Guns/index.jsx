import "./index.scss"
import Stand from "../Stand";
import {useEffect, useState} from "react";
import BigStand from "../BigStand"
import PropTypes from 'prop-types';
import Spinner from 'react-bootstrap/Spinner';
import {getColorStar} from "../../my-function"
import Footer from "../Footer";

const Guns = ({addToCart, currencyUnit, colorStar, setFavorite}) => {

    const   [stands, setStands] = useState([]);
    const   [load, setLoad] = useState(true);
    const   [stand, setStand] = useState({});
    const   [boolStn, setBoolStn] = useState(true);
    const   comeBack = () => setBoolStn(true);
    const    startBigStand = (stand) => {
            setStand({...stand});
            setBoolStn(false);
        };


    useEffect(() => {
            fetch('/source/source1.html')
                .then(response => response.json())
                .then(stands => {
                    setTimeout(() => {
                        setStands(stands)
                    }, 1500)
                    setTimeout(() => {
                        setLoad(false)
                    }, 1400)
                })
    }, [])

    if (load) {
        return (
            <div className="my-spinner">
                <Spinner animation="grow"/>
            </div>
        )
    }

    return (
        boolStn ? <>
                <main className="main container-fluid">
                    <p className="main--trek">Guns & Rifle</p>
                    <div className="main--case">
                        {stands.map(stand => (
                            <Stand
                                stand={stand}
                                key={stand.article}
                                starColor={getColorStar(stand.article) || colorStar[stand.article] || 'black'}
                                currencyUnit={currencyUnit}
                                addToCart={_ => addToCart(stand)}
                                watchMore={_ => startBigStand(stand)}
                                setFavorite={_ => setFavorite(stand)}
                            />
                        ))}
                    </div>
                </main>
                <Footer/>
            </>
            :
            <>
                <BigStand
                    stand={stand}
                    currencyUnit={currencyUnit}
                    starColor={getColorStar(stand.article) || colorStar[stand.article] || 'black'}
                    addToCart={_ => addToCart(stand)}
                    comeBack={comeBack}
                    setFavorite={_ => setFavorite(stand)}
                />
                <Footer/>
            </>
    )
}

export default Guns

Guns.propTypes = {
    addToCart: PropTypes.func,
    setFavorite: PropTypes.func,
    currencyUnit: PropTypes.string,
    colorStar: PropTypes.array,
};