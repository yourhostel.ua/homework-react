import {configureStore} from "@reduxjs/toolkit";
import {persistReducer} from "redux-persist";
import {combineReducers} from "@reduxjs/toolkit";
import storage from 'redux-persist/lib/storage';


const initialState = {
    colorStar: [],
    counterProduct: []
}

const rootReducer = (state = initialState, action) => {
    if (action.type === 'colorStar/set') {
        return {
            ...state,
            colorStar: action.payload
        }
    }
    if (action.type === 'counterProduct/set') {
        return {
            ...state,
            counterProduct: action.payload
        }
    }
    return {
        ...state
    }
}

const persistConfig = {
    key: "root",
    version: 1,
    storage
}

const reducer = combineReducers({
    reducer: rootReducer
})

const persistedReducer = persistReducer(persistConfig,reducer)

export const store = configureStore({
    reducer: persistedReducer
})

