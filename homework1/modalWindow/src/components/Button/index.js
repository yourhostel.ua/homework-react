import {Component} from 'react';
import "./index.scss";

export default class Button extends Component {
    render = () => <button
        data-specific={this.props.specific}
        className={this.props.btnClass}
        onClick={this.props.functionOnClick}
    >{this.props.btnText}</button>
}

