export function closeAnimate(item) {
    document.querySelector(`.${item}`)?.animate([
        {transform: "translateY(0%)"},
        {transform: "translateY(-110%) "},
    ], {duration: 600, iterations: 1});
}

export function lengthStorage() {
    let count= 0
    for(let key in localStorage) {
        if (!localStorage.hasOwnProperty(key)) continue;
        if (Number(key)) count++;
    }
    return count
}

export function lengthStorageProduct() {
    let count= 0
    for(let key in localStorage) {
        if (!localStorage.hasOwnProperty(key)) continue;
        if (key.match("product")) count++;
    }
    return count
}