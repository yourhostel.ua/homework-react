import "./index.scss"
import Stand from "../Stand";
import {useEffect, useState} from "react";
import BigStand from "../BigStand"
import PropTypes from 'prop-types';
import Spinner from 'react-bootstrap/Spinner';

const Main = ({addToCart, currencyUnit, colorStar, setFavorite}) => {

  const [stands, setStands] = useState([]),
        [load, setLoad] = useState(true),
        [standObj, setStandObj] = useState({}),
        [boolStn, setBoolStn] = useState(true),
        comeBack = () => setBoolStn(true),
        getFavorite = id => localStorage.getItem(id),
        startBigStand = (standObj) => {
            setStandObj({...standObj});
            setBoolStn(false);
        };


    useEffect(() => {
        fetch('/source/source1.html')
            .then(response => response.json())
            .then(stands => {
                setTimeout(() => {
                    setStands(stands)
                }, 1500)
                setTimeout(() => {
                    setLoad(false)
                }, 1400)
            })
    }, [])

    if (load) {
        return (
            <div className="my-spinner">
                <Spinner animation="grow" />
            </div>
        )
    }

    return (
        boolStn ? <main className="main">
                <p className="main--trek">Guns & Rifle</p>
                <div className="main--case">
                    {stands.map(stand => (
                        <Stand
                            stand={stand}
                            key={stand.article}
                            starColor={getFavorite(stand.article) || colorStar[stand.article] || 'black'}
                            currencyUnit={currencyUnit}
                            addToCart={_ => addToCart(stand)}
                            watchMore={_ => startBigStand(stand)}
                            setFavorite={_ => setFavorite(stand.article)}
                        />
                    ))}
                </div>
            </main>
            :
            <BigStand
                standObj={standObj}
                currencyUnit={currencyUnit}
                starColor={getFavorite(standObj.article) || colorStar[standObj.article] || 'black'}
                addToCart={_ => addToCart(standObj)}
                comeBack={comeBack}
                setFavorite={_ => setFavorite(standObj.article)}
            />
    )
}

export default Main

Main.propTypes = {
    addToCart: PropTypes.func,
    setFavorite: PropTypes.func,
    currencyUnit: PropTypes.string,
    colorStar: PropTypes.array,
};