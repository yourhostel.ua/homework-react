import "./index.scss";
import PropTypes from "prop-types";

const Button =({btnClass, functionOnClick, btnText})=> (
    <button className={btnClass} onClick={functionOnClick}>{btnText}</button>
    )

Button.propTypes = {
    btnText: PropTypes.string,
    functionOnClick: PropTypes.func,
    btnClass: PropTypes.string
};

export default Button