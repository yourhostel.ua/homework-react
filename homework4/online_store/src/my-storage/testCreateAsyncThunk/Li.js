export const Li=({_})=>(
    <li>
        <p>{_.description}</p>
        <p>{_.price}</p>
    </li>
)